<?php

namespace App\Http\Controllers;

use App\Core\Log;
use ModulusPHP\Http\Rest;
use ModulusPHP\Http\Status;
use App\Http\Controllers\Controller;
use ModulusPHP\Http\Requests\Request;

class ApiController extends Controller
{
  /*
  |--------------------------------------------------------------------------
  | Api Controller
  |--------------------------------------------------------------------------
  |
  | This is the default Api Controller. Feel free to modify it.
  |
  */
}