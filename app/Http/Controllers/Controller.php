<?php

namespace App\Http\Controllers;

use ModulusPHP\Http\Controllers\BaseController;

class Controller extends BaseController
{
  /*
  |--------------------------------------------------------------------------
  | Application Controller
  |--------------------------------------------------------------------------
  |
  | This Application Controller extends the modulusPHP Base Controller.
  | You can add new methods which will be available on all controllers.
  |
  */
}