<?php

/*
|--------------------------------------------------------------------------
| Application Services
|--------------------------------------------------------------------------
|
| This is where you should add configurations for your services.
|
*/

return [

  'logic' => [
    'username' => env('LOGIC_USERNAME'),
    'secret' => env('LOGIC_SECRET')
  ]

];