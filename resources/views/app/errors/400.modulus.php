{% partials('layouts.error') %}

{% in('title') %}
  400 Bad Request | modulusPHP
{% endin %}

{% in('main') %}

  <div class="flex--center position--ref full--height">
    <div class="__content">
      <div class="__title __m-b-md">
        400 Bad Request
      </div>
    </div>
  </div>

{% endin %}
