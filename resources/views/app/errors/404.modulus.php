{% partials('layouts.default') %}

{% in('title') %} 404 Not Found | modulusPHP - A lightweight MVC Framework by SovTech {% endin %}

{% in('main') %}

  <not-found documentation="{{ env('APP_DOC') }}"></not-found>

{% endin %}