{% partials('layouts.error') %}

{% in('title') %}
  Session has expired | modulusPHP
{% endin %}

{% in('main') %}

  <div class="flex--center position--ref full--height">
    <div class="__content">
      <div class="__title __m-b-md">
        Session has expired
      </div>
    </div>
  </div>

{% endin %}
