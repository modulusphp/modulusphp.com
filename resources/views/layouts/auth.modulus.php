<!doctype html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name='csrf-token' content="{{ $csrf_token }}"/>
  <title>{% tag("title") %}</title>
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="shortcut icon" href="/favicon.png" type="image/x-icon">
  <link rel="stylesheet" href="{{ mix('css/app.css') }}">
</head>
<body>

  <div class="container">
    {% tag("main") %}
  </div>

</body>
</html>