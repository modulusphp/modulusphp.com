
require('./bootstrap');

Vue.component('home', require('./components/app/home.vue'));
Vue.component('not-found', require('./components/app/404.vue'));

require('./components/prism/prism-markup-templating.min.js');
require('./components/prism/prism-php.min');
require('./components/prism/prism-twig.min');

const app = new Vue({
  el: '#app'
});