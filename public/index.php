<?php

if (require_once __DIR__ . '/../bootstrap/app.php') {
  $app = new App;
  $app->boot();
}