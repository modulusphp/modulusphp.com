# modulusPHP

## Getting Starterd

### Importing modulusphp.com

To import the modulusphp repo, run the following command.

```
git clone git@bitbucket.org:modulusphp/modulusphp.com.git
```

## Setting up the environment

Rename `.env.example` to `.env`

```
# Application
APP_NAME=modulusPHP
APP_ENV=local
APP_DEBUG=true
APP_URL=http://localhost
APP_ROOT=/public

# Database
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=modulusphp
DB_USERNAME=root
DB_PASSWORD=secret

# Mail
MAIL_USERNAME=example@domain.com
MAIL_PASSWORD=secret
MAIL_FROM_NAME=foo
MAIL_HOST=smtp.domain.com
MAIL_PORT=465 # or 587
MAIL_SMTP_SECURE=ssl # or tls

# C Modulus
C_MODULUS_ENABLE=false
```

Make sure, you have set the **"DB_DATABASE"**, **"DB_USERNAME"** and the **"DB_PASSWORD"**.

### Updating the .env

local

```
APP_NAME=modulusPHP
APP_ENV=local
APP_DEBUG=true
APP_URL=http://localhost
APP_ROOT=/public
APP_DOC=
```

Production

```
APP_NAME=modulusPHP
APP_ENV=production
APP_DEBUG=false
APP_URL=https://www.modulusphp.com
APP_ROOT=/public
APP_DOC=https://docs.modulusphp.com
```

## Getting the application ready

### Composer

Install all the dependencies

```
composer install
```

### Node.js NPM

Make sure you have nodejs and npm installed then install the dependencies using `npm install`.

```
npm install
```

### Migrations

This part is optional (but recommended). The following command will create a users table. (You will be able to edit the table later if you want to make any changes).

```
php modulus migrate
```

*This will create a users, password_resets and migrations table*

## Running the application

Run the following command to boot up your Application.

```
php modulus serve
```

If port `8000` is already in use, just set your own port. e.g

```
php modulus serve 8001
```

*You can now visit your Application on `http://localhost:<port>`*